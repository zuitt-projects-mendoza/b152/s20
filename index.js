let assets = [

	{
		id: "item-1",
		name: "Construction Crane",
		description: "Heavy lifting machine",
		stock: 5,
		isAvailable: true,
		dateAdded: "July 7, 2019"
	},
	{
		id: "item-2",
		name: "Backhoe",
		description: "Heavy Machinery",
		stock: 2,
		isAvailable: true,
		dateAdded: "May 1, 2004"
	}
];

console.log(assets);

let jsonSample = `{

	"sampleKey1": "valueA",
	"sampleKey2": "valueB",
	"sampleKey3": 1,
	"sampleKey4": true
	
}`

console.log(typeof jsonSample);

let jsonConvert = JSON.parse(jsonSample);
console.log(typeof jsonConvert);
console.log(jsonConvert);

let batches = [

	{
		batch: 'Batch 152'
	},
	{
		batch: 'Batch 156'
	}
];

let batchesJSON = JSON.stringify(batches);
console.log(batchesJSON);

let data = {
	name: "Katniss",
	age: 20,
	address: {
		city: "Kansas City",
		state: "Kansas"
	}
}

let dataJSON = JSON.stringify(data);
console.log(dataJSON);

let data2 = {

	username: "",
	password: "",
	isAdmin: false
}

let data3 = {

	username: "lightYagami",
	password: "notKira",
	isAdmin: false
}

let data4 = {

	username: "Llawliett",
	password: "yagamiiskira07",
	isAdmin: false
}

let data2JSON = JSON.stringify(data2);
let data3JSON = JSON.stringify(data3);
let data4JSON = JSON.stringify(data4);
let jsonArray = JSON.stringify(assets);

console.log(data2JSON);
console.log(data3JSON);
console.log(data4JSON);
console.log(jsonArray);

let items = `[

	{
		"id": "shop-1",
		"name": "Oreos",
		"stock": 5,
		"price": 50
	},
	{
		"id": "shop-2",
		"name": "Doritos",
		"stock": 10,
		"price": 150
	}
]`

let itemsJSArr = JSON.parse(items);
console.log(itemsJSArr);

itemsJSArr.pop();
console.log(itemsJSArr);

items = JSON.stringify(itemsJSArr);
console.log(items);

let courses = `[

	{
		"name": "Math 101",
		"description": "learn the basics of Math.",
		"price": 2500
	},
	{
		"name": "Science 101",
		"description": "learn the basics of Science.",
		"price": 2500
	}

]`

console.log(courses);

let coursesJSArr = JSON.parse(courses);
coursesJSArr.pop();

console.log(coursesJSArr);

courses = JSON.stringify(coursesJSArr);

console.log(courses);

let coursesJSArr2 = JSON.parse(courses);

console.log(coursesJSArr2);

coursesJSArr2.push({
		"name": "English 101",
		"description": "learn the basics of English.",
		"price": 2500
	})

console.log(coursesJSArr2);

courses = JSON.stringify(coursesJSArr2);

console.log(courses);
