let courses = [

	{
		id: "MT101",
		name: "Math 1",
		description: "Algebra",
		price: 100,
		isActive: false
	},

];

function postCourse(id,name,description,price){
		
			courses.push({

				id: id,
				name: name,
				description: description,
				price: price,
				isActive: false

			})

			alert(`You have created ${name}. The price is ${price}`)

		}


	postCourse("MT102","Math 2","Trigonometry",200);
	postCourse("MT105","Math 5","Geometry",400);
	console.log(courses);

function getSingleCourse(idInput){

	console.log(idInput);

	let foundCourse = courses.find((course)=>{

		return course.id === idInput
	})

	if(foundCourse !== undefined){
		console.log(foundCourse)
	} else {
		console.log("Invalid Credentials. No Matching Course Found.")
	}

}

getSingleCourse("MT105")

function deleteCourse(){
	courses.pop();
}

deleteCourse();

console.log(courses);